class AddNodeIdToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :node_id, :integer
  end
end
