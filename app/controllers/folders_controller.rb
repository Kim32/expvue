class FoldersController < ApplicationController
  def create
    parent = Folder.find_by_id(params[:node_id])
    Folder.create(label: params[:folder_label], parent: parent)
    render :json => Folder.all.arrange_serializable
  end
end